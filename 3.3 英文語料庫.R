#3.3 英文語料庫

library(tm)
library(tmcn)
library(magrittr)
library(wordcloud2)
library(magrittr)


####3.3.1 讀取文件與檢視語料庫####
#範例3-8
Corpus_english <- VCorpus(DirSource( "~/各單元data/ch3/tm_english",encoding = "UTF-8"))
Corpus_english
#函數inspect查看語料庫訊息
inspect( Corpus_english )

#範例3-9
#函數meta查看單筆文件的詳細訊息
meta(Corpus_english[[1]])

#範例3-10
#函數as.character查看單筆文件
as.character(Corpus_english[[1]])
#搭配函數lapply查看多筆文件
lapply( Corpus_english , as.character)

#範例3-11
#尋找出現特定單字的文件
tm_filter( Corpus_english, FUN = function(x) any(grep("Boeing 747", content(x)))) %>% 
  lapply( as.character )


####3.3.2 數據清理####
#範例3-12
#查詢tm套件，數據清理可使用的函數
getTransformations()

#範例3-13
Corpus_english_A <- tm_map( Corpus_english , stripWhitespace)
lapply( Corpus_english_A , as.character)

#範例3-14
Corpus_english_B <- tm_map( Corpus_english_A , removeNumbers)
lapply( Corpus_english_B , as.character)

#範例3-15
Corpus_english_C <- tm_map( Corpus_english_B , removePunctuation)
lapply( Corpus_english_C , as.character)

#範例3-16
Corpus_english_D <- tm_map( Corpus_english_C , removeWords , c("The New York Times"))
lapply( Corpus_english_D , as.character)

#範例3-17
#顯示前20筆英文停用詞
head( stopwords("english") ,20)
Corpus_english_E <- tm_map( Corpus_english_D , removeWords , stopwords("english"))
lapply( Corpus_english_E , as.character)

#範例3-18
install.packages("SnowballC")
Corpus_english_F <- tm_map( Corpus_english_E , stemDocument)
lapply( Corpus_english_F , as.character)


####3.3.3 練習：讀取文件與數據清理####

rm(list=ls())  #刪除所有變項
Corpus_NEWS <- VCorpus(DirSource( "~/各單元data/ch3/tm_Taiwan_NEWS",encoding = "UTF-8")) %>% 
  tm_map( stripWhitespace) %>% 
  tm_map( removeNumbers) %>% 
  tm_map( removePunctuation) %>% 
  tm_map( removeWords , stopwords("english")) %>% 
  tm_map( stemDocument)
lapply( Corpus_NEWS , as.character) %>% head(1)


####3.3.4 詞匯文檔矩陣####
#範例3-19
#以文件檔名為行，關鍵字為列
NEWS_tdm <- TermDocumentMatrix(Corpus_NEWS)
inspect(NEWS_tdm)

#範例3-20
#以關鍵字為行，文件檔名為列
NEWS_dtm <- DocumentTermMatrix(Corpus_NEWS)
inspect(NEWS_dtm)

#範例3-21
tdm_matrix <- as.matrix(NEWS_tdm)
tdm_sum <- rowSums(tdm_matrix)
head(tdm_sum , 20)

#範例3-22
tdm_table <- table(tdm_sum)
head( tdm_table )
tail( tdm_table )

#範例3-23
tdm_sort <- sort( tdm_sum , decreasing = T ) 
head(tdm_sort)

#範例3-24
findFreqTerms(NEWS_dtm, lowfreq = 30, highfreq = 35)
findFreqTerms(NEWS_dtm, lowfreq = 100)

#範例3-25
dim(NEWS_tdm)
NEWS_sparse <- removeSparseTerms(NEWS_tdm, sparse = 0.9)
dim(NEWS_sparse)
inspect(NEWS_sparse)

#範例3-26
findAssocs(NEWS_tdm, "democraci", corlimit = 0.7)


####3.3.5 繪製文字雲####
#範例3-27
tdm_freq <- as.matrix(NEWS_tdm) %>% 
  rowSums %>% 
  sort( decreasing = T ) 
tdm_data <- data.frame( word = names(tdm_freq) , freq = tdm_freq )
head( tdm_data )

#範例3-28
wordcloud2( tdm_data )

#範例3-29
#將字體大小與間距縮小，文字雲形狀會更明顯
wordcloud2( tdm_data , size = 0.8 , gridSize = 0.8 , shape = 'star')

#範例3-30
#若要所有文字旋轉角度相同，才使用rotateRatio = 1
wordcloud2( tdm_data , minSize = 12 , 
            fontFamily = "Arial" , minRotation = -pi/6, 
            maxRotation = -pi/6, rotateRatio = 1)

#範例3-31
#字體正常粗細，亮系顏色，灰色背景
wordcloud2( tdm_data , minSize = 12 , fontWeight = "normal" ,
            color = "random-light" , backgroundColor = "gray")

#範例3-32
#黑白圖檔，wordcloud2_data資料夾有Twitter與Batman可供選擇
wordcloud2(tdm_data, size = 1.2 ,gridSize = 1.5 ,
           figPath = "~/各單元data/ch3/wordcloud2_data/Batman.png")

#範例3-33
letterCloud(tdm_data, "R", wordSize = 0.3)

