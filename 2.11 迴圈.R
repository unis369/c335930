#2.11 迴圈

####2.11.1 for迴圈####
#範例2-96
for ( i in 1:5) {
  print( i )
}

#範例2-97
fruit <- c("apple","banana","cherry","grape","lemon")
for ( i in fruit) {
  print( i )
}

#範例2-98
for ( i in 1:10) {
  if( i == 3 )  {
    next
  }
  if( i == 8 )  {
    break
  }
  print( i )
}


####2.11.2 while迴圈####
#範例2-99
x <- 0
while ( x != 5) {
  x <- x + 1
  print(x)
}

#範例2-100
x <- 0
while ( x < 10 ) {
  x <- x + 1
  if (x == 2) next
  if (x == 7) break
  print(x)
}


####2.11.3 repeat迴圈####
#範例2-101
x <- 0
repeat{
  x <- x + 1
  print( x )
  if( x == 5 ) break
}
